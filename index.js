var express = require('express');
var socket = require('socket.io');
var mysql = require('mysql');

// Database Connection 
var con = mysql.createConnection({
  host: "127.0.0.1",
  user: "root",
  password: "",
  database : "nodedb"
});
con.connect(function(err) { 
    if (err) throw err;
    console.log("Connected!");
});
// App setup
var app = express();
var server = app.listen(4000, function(){
    console.log('listening for requests on port 4000,');
});

// Static files
app.use(express.static('public'));

// Socket setup & pass server
var io = socket(server);
io.on('connection', (socket) => {

    console.log('made socket connection', socket.id);

    socket.on('init_chat',function(){ 
        con.query("SELECT * FROM posts", function (err, result, fields) {
            if (err) throw err;
            io.sockets.emit('init_chat',result); 
        });
    });

    // Handle chat event
    socket.on('chat', function(data){
        // console.log(data);
        io.sockets.emit('chat', data);
        var sql = "INSERT INTO posts (title, body) VALUES ('"+data.handle+"', '"+data.message+"')";
        con.query(sql, function (err, result) {
            if (err) throw err;
            console.log("1 record inserted");
        });
    });

    // Handle typing event
    socket.on('typing', function(data){
        socket.broadcast.emit('typing', data);
    });

});
