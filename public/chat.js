// Make connection
var socket = io.connect('http://localhost:4000');
// var mysql = require('mysql');

// // Database Connection 
// var con = mysql.createConnection({
//   host: "127.0.0.1",
//   user: "root",
//   password: "",
//   database : "nodedb"
// });

// con.connect(function(err) {
//   if (err) throw err;
//   console.log("Connected!");
//   con.query("SELECT * FROM posts", function (err, result, fields) {
//     if (err) throw err;
//     console.log(result);
//     var output = document.getElementById('output') ; 
//     result.forEach(element => {
//         output.innerHTML += '<p><strong>' + element.title + ': </strong>' + element.body + '</p>';
//     });
//   });
// });

// Query DOM
var message = document.getElementById('message'),
      handle = document.getElementById('handle'),
      btn = document.getElementById('send'),
      output = document.getElementById('output'),
      feedback = document.getElementById('feedback');      

socket.emit('init_chat');

// Emit events
btn.addEventListener('click', function(){
    socket.emit('chat', {
        message: message.value,
        handle: handle.value
    });
    message.value = "";
});

message.addEventListener('keypress', function(){
    socket.emit('typing', handle.value);
})

// Listen for events
socket.on('chat', function(data){
    feedback.innerHTML = '';
    output.innerHTML += '<p><strong>' + data.handle + ': </strong>' + data.message + '</p>';
});

socket.on('typing', function(data){
    if(data.length > 0)
        feedback.innerHTML = '<p><em>' + data + ' is typing a message...</em></p>';
});

socket.on('init_chat', function(data){
    output.innerHTML = "" ;
    data.forEach(element => {
        output.innerHTML += '<p><strong>' + element.title + ': </strong>' + element.body + '</p>';        
    });
});

